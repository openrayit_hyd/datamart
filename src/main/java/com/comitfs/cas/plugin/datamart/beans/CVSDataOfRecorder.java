package com.comitfs.cas.plugin.datamart.beans;

public class CVSDataOfRecorder {
    private int channelId;
    private String audioFilePath;

    public CVSDataOfRecorder(int channelId, String audioFilePath) {
        this.channelId = channelId;
        this.audioFilePath = audioFilePath;
    }

    public int getChannelId() {
        return channelId;
    }

    public String getAudioFilePath() {
        return audioFilePath;
    }
}
