package com.comitfs.cas.plugin.datamart.ui;

import com.comitfs.cas.plugin.datamart.beans.CVSDataOfCallInteraction;
import com.comitfs.cas.plugin.datamart.beans.CVSDataOfInteraction;
import com.comitfs.cas.plugin.datamart.beans.CVSDataOfParticipant;
import com.comitfs.cas.plugin.datamart.beans.CVSDataOfRecorder;
import com.comitfs.cas.plugin.datamart.dao.MSSQLDao;
import com.comitfs.cas.plugin.datamart.dao.MYSQLDao;
import com.comitfs.cas.plugin.datamart.mysqlbean.CVSBean;
import com.comitfs.cas.plugin.datamart.util.DMartConstants;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@SuppressWarnings("serial")
public class DMartServlet extends HttpServlet {
    private static Logger Log = LoggerFactory.getLogger(DMartServlet.class);

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        super.init(servletConfig);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletOutputStream out = resp.getOutputStream();
        List<CVSBean> cvs = null;
        MSSQLDao mssqlDao = new MSSQLDao();
        MYSQLDao mysqlDao = new MYSQLDao();

        try {
            Set<String> interactionTableNames = mssqlDao.getInteractionTableNames();
            for (String interactionTableName : interactionTableNames) {
                try {
                    List<CVSDataOfInteraction> cvsDataOfIntrTable = mssqlDao.getInteractionDataForCVSTable(interactionTableName);
                    constructCVSRecordAndInsert(cvsDataOfIntrTable, mssqlDao, mysqlDao, fetchTableSuffix(interactionTableName));
                } catch (Exception e) {
                    Log.error("Failed inserting DMart data of table :" + interactionTableName + " into CVS", e);
                }
            }
        } catch (Exception e) {
            Log.error("Failed executing DataMart data to CVS DB", e);
        }
    }

    private void constructCVSRecordAndInsert(List<CVSDataOfInteraction> cvsDataOfIntrTable, MSSQLDao mssqlDao,
                                             MYSQLDao mysqlDao, String tableSuffix) {
        for (CVSDataOfInteraction cvsDataOfInteraction : cvsDataOfIntrTable) {
            try {
                long interactionId = cvsDataOfInteraction.getInteractionId();
                CVSDataOfParticipant cvsDataOfParticipant = fetchCVSDataOfParticipant(interactionId, mssqlDao, tableSuffix);
                CVSDataOfRecorder cvsDataOfRecorder = fetchCVSDataOfRecording(interactionId, mssqlDao, tableSuffix);
                CVSDataOfCallInteraction cvsDataOfCallInteraction = fetchCVSDataOfCallInteraction(interactionId, mssqlDao, tableSuffix);
                int duration = fetchCVSDataOfClipDuration(interactionId, mssqlDao, tableSuffix);
                String audioFilePath = cvsDataOfRecorder.getAudioFilePath();

                String recordType = "VOX";
                if(StringUtils.isBlank(audioFilePath)) {
                    recordType = "CDR";
                }

                int pbxCallId = cvsDataOfCallInteraction.getPbxCallId();
                if (pbxCallId == -1) {
                    continue;
                }

                CVSBean cvsBean = new CVSBean(cvsDataOfParticipant.getUserID(), cvsDataOfRecorder.getChannelId(),
                        cvsDataOfParticipant.getPhoneNumber(),
                        cvsDataOfCallInteraction.getCallDirectionTypeId(),String.valueOf(cvsDataOfParticipant.getDeviceID()),
                        cvsDataOfInteraction.getStartTimestamp(), cvsDataOfInteraction.getEndTimestamp(),duration,
                        audioFilePath,recordType, String.valueOf(pbxCallId));
                mysqlDao.insertRecords(cvsBean);
            } catch (Exception e) {
                Log.error("Error reading CVS related data of " + cvsDataOfInteraction, e);
            }
        }
    }

    private int fetchCVSDataOfClipDuration(long interactionId, MSSQLDao mssqlDao, String tableSuffix) {
        return mssqlDao.getClipDurationData(tableSuffix, interactionId);
    }

    private CVSDataOfCallInteraction fetchCVSDataOfCallInteraction(long interactionId, MSSQLDao mssqlDao, String tableSuffix) {
        return mssqlDao.getCallInteractionData(tableSuffix, interactionId);
    }

    private CVSDataOfRecorder fetchCVSDataOfRecording(long interactionId, MSSQLDao mssqlDao, String tableSuffix) {
        return mssqlDao.getRecordingData(tableSuffix, interactionId);
    }

    private CVSDataOfParticipant fetchCVSDataOfParticipant(long interactionId, MSSQLDao mssqlDao, String tableSuffix) {
        return mssqlDao.getParticipantData(tableSuffix, interactionId);
    }

    private String fetchTableSuffix(String interactionTable) {
        //TODO - Implement regex
        if (null != interactionTable && !"".equals(interactionTable)) {
            return interactionTable.substring(DMartConstants.INTERACTION_TABLE_PREFIX.length());
        }
        return null;
    }
}
