package com.comitfs.cas.plugin.datamart.beans;

public class Directionbean {

    private int tiDirectionTypeID;
    private long iInteractionID;
    private String vcDirectionTypeDesc;

    public long getiInteractionID() {
        return iInteractionID;
    }

    public void setiInteractionID(long iInteractionID) {
        this.iInteractionID = iInteractionID;
    }

    public int getTiDirectionTypeID() {
        return tiDirectionTypeID;
    }

    public void setTiDirectionTypeID(int tiDirectionTypeID) {
        this.tiDirectionTypeID = tiDirectionTypeID;
    }

    public String getVcDirectionTypeDesc() {
        return vcDirectionTypeDesc;
    }

    public void setVcDirectionTypeDesc(String vcDirectionTypeDesc) {
        this.vcDirectionTypeDesc = vcDirectionTypeDesc;
    }

    @Override
    public String toString() {
        return "Directionbean [tiDirectionTypeID=" + tiDirectionTypeID + ", vcDirectionTypeDesc=" + vcDirectionTypeDesc
                + "]";
    }


}
