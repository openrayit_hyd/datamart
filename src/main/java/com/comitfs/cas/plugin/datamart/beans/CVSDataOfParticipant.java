package com.comitfs.cas.plugin.datamart.beans;

public class CVSDataOfParticipant {
    private int userID;
    private int deviceID;
    private String phoneNumber;
    private String station;

    public CVSDataOfParticipant(int userID, int deviceID, String phoneNumber, String nvcStation) {
        this.userID = userID;
        this.deviceID = deviceID;
        this.phoneNumber = phoneNumber;
        this.station = nvcStation;
    }

    public int getUserID() {
        return userID;
    }

    public int getDeviceID() {
        return deviceID;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getStation() {
        return station;
    }
}
