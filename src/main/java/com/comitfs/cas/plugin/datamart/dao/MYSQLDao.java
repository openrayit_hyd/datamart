package com.comitfs.cas.plugin.datamart.dao;

import com.comitfs.cas.plugin.datamart.mysqlbean.CVSBean;
import org.jivesoftware.database.DbConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;

public class MYSQLDao {
    private static Logger Log = LoggerFactory.getLogger(MYSQLDao.class);

    public int insertRecords(CVSBean cvsBean) {
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            connection = DbConnectionManager.getConnection();
            String sql = "insert into cvs(CVSCID, CVSPHN, CVSDIR, CVSNBR, CVSSDT, CVSEDT, CVSSEC, CVSLCT, CVSTYP, CVSPCI) values (?,?,?,?,?,?,?,?,?,?)";
            ps = connection.prepareStatement(sql);
            ps.setInt(1, cvsBean.getChannelId());
            ps.setString(2, cvsBean.getPhoneNumber());
            ps.setInt(3, cvsBean.getDirection());
            ps.setString(4, cvsBean.getDeviceNumber());
            ps.setTimestamp(5, cvsBean.getStartTime());
            ps.setTimestamp(6, cvsBean.getEndTime());
            ps.setInt(7, cvsBean.getDuration());
            ps.setString(8, cvsBean.getLocation());
            ps.setString(9, cvsBean.getRecordType());
            ps.setString(10, cvsBean.getPbxCallId());
            return ps.executeUpdate();
        } catch (Exception e) {
            Log.error("Failed inserting record into CVS table ", e);
        } finally {
            try {
                DbConnectionManager.closeConnection(ps, connection);
            } catch (Exception e) {

            }
        }
        return -1;
    }
}

