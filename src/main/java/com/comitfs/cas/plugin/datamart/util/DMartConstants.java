package com.comitfs.cas.plugin.datamart.util;

public class DMartConstants {
    public static final String INTERACTION_TABLE_PREFIX = "tblInteraction";
    public static final String CALL_INTERACTION_TABLE_PREFIX = "tblCallInteraction";
    public static final String PARTICIPANT_TABLE_PREFIX = "tblParticipant";
}
