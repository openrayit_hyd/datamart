package com.comitfs.cas.plugin.datamart.beans;

import java.sql.Timestamp;

//Class corresponding to tbl.Interaction
public class Interactionbean {

    private long iInteractionID;
    private Timestamp dtInteractionGMTStartTime;
    private Timestamp dtInteractionGMTStopTime;
    private int iInitiatorUserID;
    private int iSwitchID;
    private int tiInteractionRecordedTypeID;
    private int iInteractionOriginalID;

    public long getiInteractionID() {
        return iInteractionID;
    }

    public void setiInteractionID(long iInteractionID) {
        this.iInteractionID = iInteractionID;
    }

    public Timestamp getDtInteractionGMTStartTime() {
        return dtInteractionGMTStartTime;
    }

    public void setDtInteractionGMTStartTime(Timestamp dtInteractionGMTStartTime) {
        this.dtInteractionGMTStartTime = dtInteractionGMTStartTime;
    }

    public Timestamp getDtInteractionGMTStopTime() {
        return dtInteractionGMTStopTime;
    }

    public void setDtInteractionGMTStopTime(Timestamp dtInteractionGMTStopTime) {
        this.dtInteractionGMTStopTime = dtInteractionGMTStopTime;
    }

    public int getiInitiatorUserID() {
        return iInitiatorUserID;
    }

    public void setiInitiatorUserID(int iInitiatorUserID) {
        this.iInitiatorUserID = iInitiatorUserID;
    }

    public int getiSwitchID() {
        return iSwitchID;
    }

    public void setiSwitchID(int iSwitchID) {
        this.iSwitchID = iSwitchID;
    }

    public int getTiInteractionRecordedTypeID() {
        return tiInteractionRecordedTypeID;
    }

    public void setTiInteractionRecordedTypeID(int tiInteractionRecordedTypeID) {
        this.tiInteractionRecordedTypeID = tiInteractionRecordedTypeID;
    }

    public int getiInteractionOriginalID() {
        return iInteractionOriginalID;
    }

    public void setiInteractionOriginalID(int iInteractionOriginalID) {
        this.iInteractionOriginalID = iInteractionOriginalID;
    }

    @Override
    public String toString() {
        return "Interactionbean [iInteractionID=" + iInteractionID + ", dtInteractionGMTStartTime="
                + dtInteractionGMTStartTime + ", dtInteractionGMTStopTime=" + dtInteractionGMTStopTime
                + ", iInitiatorUserID=" + iInitiatorUserID + ", iSwitchID=" + iSwitchID
                + ", tiInteractionRecordedTypeID=" + tiInteractionRecordedTypeID + ", iInteractionOriginalID="
                + iInteractionOriginalID + "]";
    }


}
