package com.comitfs.cas.plugin.datamart.plugin;

import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.webapp.WebAppContext;
import org.jivesoftware.openfire.container.Plugin;
import org.jivesoftware.openfire.container.PluginManager;
import org.jivesoftware.openfire.http.HttpBindManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

public class PluginDataMart implements Plugin {
    private static Logger LOG = LoggerFactory.getLogger(PluginDataMart.class);

    @Override
    public void destroyPlugin() {
        LOG.info("DataMart plugin shutting down..");
    }

    @Override
    public void initializePlugin(PluginManager pluginManager, File pluginDirectory) {
        LOG.info("Initializing DataMart Plugin..");
        try {
            ContextHandlerCollection contexts = HttpBindManager.getInstance().getContexts();
            @SuppressWarnings("unused")
            WebAppContext context = new WebAppContext(contexts, pluginDirectory.getPath(), "/dmart");

        } catch (Exception e) {
            LOG.error("Error initializing WebAppContext for DataMart Plugin");
        }
    }
}
