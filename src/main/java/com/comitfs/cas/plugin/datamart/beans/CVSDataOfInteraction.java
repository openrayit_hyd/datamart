package com.comitfs.cas.plugin.datamart.beans;

import java.sql.Timestamp;

public class CVSDataOfInteraction {

    private long interactionId;
    private Timestamp startTimestamp;
    private Timestamp endTimestamp;

    public CVSDataOfInteraction(long interactionId, Timestamp startTimestamp, Timestamp endTimestamp) {
        this.interactionId = interactionId;
        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
    }

    public long getInteractionId() {
        return interactionId;
    }

    public Timestamp getEndTimestamp() {
        return endTimestamp;
    }

    public Timestamp getStartTimestamp() {
        return startTimestamp;
    }
}
