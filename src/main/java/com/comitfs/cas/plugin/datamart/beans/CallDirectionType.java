package com.comitfs.cas.plugin.datamart.beans;

public enum CallDirectionType {
    Unknown(0),
    Incoming(1),
    Outgoing(2),
    Internal(3),
    Block(4),
    Tandem(5),
    External(6);

    int directionTypeId;

    CallDirectionType(int directionTypeId) {
        this.directionTypeId = directionTypeId;
    }

    public static String fromTypeId(int direction) {
        for (CallDirectionType typeId : CallDirectionType.values()) {
            if (typeId.directionTypeId == direction) {
                return typeId.name();
            }
        }
        return null;
    }
}
