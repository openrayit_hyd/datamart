package com.comitfs.cas.plugin.datamart.dao;

import com.comitfs.cas.plugin.datamart.beans.CVSDataOfCallInteraction;
import com.comitfs.cas.plugin.datamart.beans.CVSDataOfInteraction;
import com.comitfs.cas.plugin.datamart.beans.CVSDataOfParticipant;
import com.comitfs.cas.plugin.datamart.beans.CVSDataOfRecorder;
import com.comitfs.cas.plugin.datamart.util.DMartConstants;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MSSQLDao {
    private static Logger LOG = LoggerFactory.getLogger(MSSQLDao.class);

    public Connection getConnection() throws ClassNotFoundException, SQLException {
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        Connection con = DriverManager.getConnection(
                "jdbc:sqlserver://192.168.3.138:1433;" + "databaseName=dm_nice_interactions;user=sa;password=Pa55w0rd");
        return con;
    }

    public Set<String> getInteractionTableNames() {
        LOG.debug("Retrieving interactions table names");
        Set<String> interactionTables = new HashSet<String>();
        try {
            DatabaseMetaData meta = getConnection().getMetaData();
            ResultSet res = meta.getTables(null, null, "%", new String[]{"TABLE"});
            while (res.next()) {
                String tableName = res.getString(3);
                Pattern pattern = Pattern.compile(DMartConstants.INTERACTION_TABLE_PREFIX + "[0-9]*$");
                Matcher matcher = pattern.matcher(tableName);
                if (matcher.matches()) {
                    interactionTables.add(tableName);
                }
            }
        } catch (Exception e) {
            LOG.error("Unable to get Interaction table names ", e);
        } finally {
            //Close DB resources
        }
        LOG.debug("Interaction tables fetched " + interactionTables);
        return interactionTables;
    }

    public CVSDataOfRecorder getRecordingData(String tableSuffix, long interactionId) {
        int channelId = -1;
        String audioFilePath = null;
        try {
            Connection con = getConnection();
            String sql = "select iChannel, nvcTextPath from dbo.tblRecording" + tableSuffix
                    + " where iInteractionID = " + interactionId;
            LOG.debug("Recording table SQL: " + sql);
            PreparedStatement pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                channelId = rs.getInt(1);
                audioFilePath = rs.getString(2);
            }
        } catch (Exception e) {
            LOG.error("Error fetching channelId and audio file path from Recording table ", e);
        } finally {
            //Close DB resources
        }
        return new CVSDataOfRecorder(channelId, audioFilePath);
    }

    public CVSDataOfParticipant getParticipantData(String tableSuffix, long interactionId) {
        int userId = -1;
        int deviceId = -1;
        String nvcPhoneNumber = null;
        String nvcStation = null;
        try {
            Connection con = getConnection();
            String sql = "select iUserID, iDeviceID, nvcPhoneNumber, nvcStation from dbo.tblParticipant" + tableSuffix + " where iInteractionID = " + interactionId;
            LOG.debug("Participant table SQL: " + sql);
            PreparedStatement pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();

            while (rs.next()) {
                int userVal = rs.getInt(1);
                if (userVal != -1) {
                    userId = userVal;
                }

                int deviceVal = rs.getInt(2);
                if (deviceVal != -1) {
                    deviceId = deviceVal;
                }

                String phoneVal = rs.getString(3);
                if (StringUtils.isNotBlank(phoneVal)) {
                    nvcPhoneNumber = phoneVal;
                }

                String stationVal = rs.getString(4);
                if (StringUtils.isNotBlank(stationVal)) {
                    nvcStation = stationVal;
                }
            }
        } catch (Exception e) {
            LOG.error("Failed getting Participant details of interactionId: " + interactionId, e);
        } finally {
            //Close DB resources
        }
        return new CVSDataOfParticipant(userId, deviceId, nvcPhoneNumber, nvcStation);
    }

    public CVSDataOfCallInteraction getCallInteractionData(String tableSuffix, long interactionId) {
        int pbxCallId = -1;
        int callDirectionId = -1;
        try {
            Connection con = getConnection();
            String sql = "select iPBXCallID, tiCallDirectionTypeID from dbo.tblCallInteraction" + tableSuffix
                    + " where iInteractionID = " + interactionId;
            LOG.debug("CallInteraction table SQL: " + sql);
            PreparedStatement pstmt = con.prepareStatement(sql);
            ResultSet rs = pstmt.executeQuery();
            while (rs.next()) {
                pbxCallId = rs.getInt(1);
                callDirectionId = rs.getInt(2);
            }
        } catch (Exception e) {
            LOG.error("Failed getting CVS data of CallInteraction table ", e);
        } finally {
            //Close DB resources
        }
        return new CVSDataOfCallInteraction(pbxCallId, callDirectionId);
    }

    public List<CVSDataOfInteraction> getInteractionDataForCVSTable(String interactionTable) {
        List<CVSDataOfInteraction> cvsInteractionData = new ArrayList<CVSDataOfInteraction>();
        try {
            Connection con = getConnection();
            String sql = "Select iInteractionID, dtInteractionGMTStartTime, dtInteractionGMTStopTime from dbo." + interactionTable;
            LOG.debug("Interaction table SQL: " + sql);
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                long intrId = resultSet.getLong(1);
                Timestamp gmtStartTime = resultSet.getTimestamp(2);
                Timestamp gmtEndTime = resultSet.getTimestamp(3);
                CVSDataOfInteraction cvsDataOfInteraction = new CVSDataOfInteraction(intrId, gmtStartTime, gmtEndTime);
                cvsInteractionData.add(cvsDataOfInteraction);
            }
        } catch (Exception e) {
            LOG.error("Unable to fetch CVS required data from Interaction table " + interactionTable, e);
        } finally {
            //close DB resources
        }
        return cvsInteractionData;

    }

    public int getClipDurationData(String tableSuffix, long interactionId) {
        try {
            Connection con = getConnection();
            String sql = "Select biClipDuration from dbo.tblClipRecordingInteraction" + tableSuffix + " where iInteractionID=" + interactionId;
            LOG.debug("ClipRecordInteraction table SQL: " + sql);
            PreparedStatement stmt = con.prepareStatement(sql);
            ResultSet resultSet = stmt.executeQuery();
            while (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (Exception e) {
            LOG.error("Failed getting Clip duration ", e);
        } finally {
            //Close DB resources
        }
        return -1;
    }
}
