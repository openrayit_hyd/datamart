package com.comitfs.cas.plugin.datamart.beans;

public class CVSDataOfCallInteraction {
    private int pbxCallId;
    private int callDirectionTypeId;

    public CVSDataOfCallInteraction(int pbxCallId, int callDirectionTypeId) {
        this.pbxCallId = pbxCallId;
        this.callDirectionTypeId = callDirectionTypeId;
    }

    public int getPbxCallId() {
        return pbxCallId;
    }

    public int getCallDirectionTypeId() {
        return callDirectionTypeId;
    }
}
