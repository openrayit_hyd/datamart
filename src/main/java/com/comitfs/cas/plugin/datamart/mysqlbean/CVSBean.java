package com.comitfs.cas.plugin.datamart.mysqlbean;

import java.sql.Timestamp;

public class CVSBean {
    int userId; //CVSUSR
    int channelId; //CVSCID
    int CVSCHN;
    int CVSCMP;
    String phoneNumber; //CVSPHN
    int CVSPHL;
    int direction; //CVSDIR
    String deviceNumber; //CVSNBR
    Timestamp startTime; // CVSSDT
    Timestamp endTime; // CVSEDT
    int duration; //CVSSEC
    String location; // CVSLCT
    int CVSSTS;
    int CVSREC;
    int CVSREM;
    String CVSGRP;
    String CVSORG;
    String recordType; //CVSTYP

    int CVSRAS;
    String pbxCallId; //CVSPCI CTI CallId
    String CVSCIP;
    String CVSCEP;
    String CVSRET;
    int IsNewForCvsProcessor;

    public CVSBean(int userId, int channelId, String phoneNumber, int direction, String deviceNumber,
                   Timestamp startTime, Timestamp endTime, int duration, String location,
                   String recordType, String pbxCallId) {
        this.userId = userId;
        this.channelId = channelId;
        this.phoneNumber = phoneNumber;
        this.direction = direction;
        this.deviceNumber = deviceNumber;
        this.startTime = startTime;
        this.endTime = endTime;
        this.duration = duration;
        this.location = location;
        this.recordType = recordType;
        this.pbxCallId = pbxCallId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public int getCVSCHN() {
        return CVSCHN;
    }

    public void setCVSCHN(int CVSCHN) {
        this.CVSCHN = CVSCHN;
    }

    public int getCVSCMP() {
        return CVSCMP;
    }

    public void setCVSCMP(int CVSCMP) {
        this.CVSCMP = CVSCMP;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getCVSPHL() {
        return CVSPHL;
    }

    public void setCVSPHL(int CVSPHL) {
        this.CVSPHL = CVSPHL;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public String getDeviceNumber() {
        return deviceNumber;
    }

    public void setDeviceNumber(String deviceNumber) {
        this.deviceNumber = deviceNumber;
    }

    public Timestamp getStartTime() {
        return startTime;
    }

    public void setStartTime(Timestamp startTime) {
        this.startTime = startTime;
    }

    public Timestamp getEndTime() {
        return endTime;
    }

    public void setEndTime(Timestamp endTime) {
        this.endTime = endTime;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getCVSSTS() {
        return CVSSTS;
    }

    public void setCVSSTS(int CVSSTS) {
        this.CVSSTS = CVSSTS;
    }

    public int getCVSREC() {
        return CVSREC;
    }

    public void setCVSREC(int CVSREC) {
        this.CVSREC = CVSREC;
    }

    public int getCVSREM() {
        return CVSREM;
    }

    public void setCVSREM(int CVSREM) {
        this.CVSREM = CVSREM;
    }

    public String getCVSGRP() {
        return CVSGRP;
    }

    public void setCVSGRP(String CVSGRP) {
        this.CVSGRP = CVSGRP;
    }

    public String getCVSORG() {
        return CVSORG;
    }

    public void setCVSORG(String CVSORG) {
        this.CVSORG = CVSORG;
    }

    public String getRecordType() {
        return recordType;
    }

    public void setRecordType(String recordType) {
        this.recordType = recordType;
    }

    public int getCVSRAS() {
        return CVSRAS;
    }

    public void setCVSRAS(int CVSRAS) {
        this.CVSRAS = CVSRAS;
    }

    public String getPbxCallId() {
        return pbxCallId;
    }

    public void setPbxCallId(String pbxCallId) {
        this.pbxCallId = pbxCallId;
    }

    public String getCVSCIP() {
        return CVSCIP;
    }

    public void setCVSCIP(String CVSCIP) {
        this.CVSCIP = CVSCIP;
    }

    public String getCVSCEP() {
        return CVSCEP;
    }

    public void setCVSCEP(String CVSCEP) {
        this.CVSCEP = CVSCEP;
    }

    public String getCVSRET() {
        return CVSRET;
    }

    public void setCVSRET(String CVSRET) {
        this.CVSRET = CVSRET;
    }

    public int getIsNewForCvsProcessor() {
        return IsNewForCvsProcessor;
    }

    public void setIsNewForCvsProcessor(int isNewForCvsProcessor) {
        IsNewForCvsProcessor = isNewForCvsProcessor;
    }
}
